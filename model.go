package main

// postgreSQL TABLE structure
type UserInfo struct {
	Userid int	`pg:"userid,pk"`
	Name string	`json:"name"`
	Liked []int	`pg:",array"`
	Disliked []int	`pg:",array"`
	BeLiked []int	`pg:",array"`
	Match []int	`pg:",array"`
}

func (user *UserInfo) AddUser () error {
	err := Db.Insert(user)
	if err != nil {
		return err
	}
	return nil
}

func (user *UserInfo) GetUserById () error {
	err := Db.Select(user)
	if err != nil {
		return err
	}
	return nil
}

func (user *UserInfo) UpdateUser() {
	err := Db.Update(user)
	if err != nil {

	}
}

func GetAllUsers () []UserInfo {
	var allUsers []UserInfo
	err := Db.Model(&allUsers).Select()
	if err != nil {

	}
	return allUsers
}

// find the index of an item in a slice
func Contains(item int, s []int) (index int) {
	index = -1
	for i := range s{
		if s[i] == item {
			index = i
			return
		}
	}
	return
}