package main

import (
	"github.com/go-pg/pg/v9"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func InitDb()  {
	Db = pg.Connect(&pg.Options{
		User: "liningning",
		Addr:"localhost:5432",
		Database:"liningning",
	})
	logger.Println("db connection created")
}

func InitLogger()  {
	logFile, err := os.Create("api.log")
	if err != nil {
		log.Fatal(err.Error())
	}
	logger = log.New(logFile, "", log.Ldate|log.Ltime|log.Lshortfile)
}

func init()  {
	InitLogger()
	InitDb()
}

// initial a global db connection, its safe for concurrence
var logFile *os.File
var logger *log.Logger
var Db *pg.DB

func main()  {
	// close connection when main fun is done
	defer Db.Close()
	defer logFile.Close()

	router := mux.NewRouter()
	router.HandleFunc("/users", AddUserHandler).Methods("POST")
	router.HandleFunc("/users", GetAllUsersHandler).Methods("GET")
	router.HandleFunc("/users/{user_id}/relationships", GetUserHandler)
	router.HandleFunc("/users/{user_id}/relationships/{other_user_id}", MakeRelationHandler).Methods("PUT")
	server := http.Server{
		Addr:			"127.0.0.1:8080",
		Handler:		router,
	}
	log.Fatal(server.ListenAndServe())
}