package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)


// define relationship
const (
	Matched string = "matched"
	Liked string = "liked"
	Disliked string = "disliked"
)

const (
	RelationShipType string = "relationship"
	UserType string = "user"
)


// Create a user
func AddUserHandler(w http.ResponseWriter, r *http.Request) {
	logger.Println(r.URL)
	decoder := json.NewDecoder(r.Body)
	var u UserInfo
	_ = decoder.Decode(&u)

	if len(u.Name) > 100 {
		reason := fmt.Sprintf("user name too long: %s\n", u.Name)
		json.NewEncoder(w).Encode(reason)
		return
	}

	u.AddUser()
	// make return json
	re := make(map[string]string)
	re["id"] = strconv.Itoa(u.Userid)
	re["name"] = u.Name
	re["type"] = UserType
	err := json.NewEncoder(w).Encode(re)
	if err != nil {
	}
}

// get all relationships of a user
func GetUserHandler(w http.ResponseWriter, r *http.Request)  {
	logger.Println(r.URL)
	vars := mux.Vars(r)
	userid, _ := strconv.Atoi(vars["user_id"])
	var u = UserInfo{Userid:userid}
	err := u.GetUserById()
	if err != nil {
		reason := fmt.Sprintf("cannot get user by id %s\n", vars["user_id"])
		json.NewEncoder(w).Encode(reason)
		return
	}

	// Declare a variables
	re := make([]map[string]string, 0, 0)
	for i := range u.Liked{
		_user := make(map[string]string)
		_user["id"] = strconv.Itoa(u.Liked[i])
		_user["state"] = Liked
		_user["type"] = RelationShipType
		re = append(re, _user)
	}
	for i := range u.Disliked{
		_user := make(map[string]string)
		_user["id"] = strconv.Itoa(u.Disliked[i])
		_user["state"] = Disliked
		_user["type"] = RelationShipType
		re = append(re, _user)
	}
	for i := range u.Match{
		_user := make(map[string]string)
		_user["id"] = strconv.Itoa(u.Match[i])
		_user["state"] = Matched
		_user["type"] = RelationShipType
		re = append(re, _user)
	}

	json.NewEncoder(w).Encode(re)
}

// Return all exists users
func GetAllUsersHandler(w http.ResponseWriter, r *http.Request) {
	logger.Println(r.URL)
	allUsers := GetAllUsers()
	re := make([]map[string]string, 0, 0)
	for i := range allUsers{
		currentUser := allUsers[i]
		_user := make(map[string]string)
		_user["id"] = strconv.Itoa(currentUser.Userid)
		_user["name"] = currentUser.Name
		_user["type"] = UserType
		re = append(re, _user)
	}

	json.NewEncoder(w).Encode(re)
}

// Create or Update a relation between two users
func MakeRelationHandler(w http.ResponseWriter, r *http.Request) {
	logger.Println(r.URL)
	var u1, u2 UserInfo
	vars := mux.Vars(r)
	Userid, err := strconv.Atoi(vars["user_id"])
	if err != nil {
		fmt.Println(err)
	}
	u1.Userid = Userid
	err = u1.GetUserById()
	if err != nil {
		reason := fmt.Sprintf("cannot get user by id %d\n", u1.Userid)
		json.NewEncoder(w).Encode(reason)
		return
	}

	OtherUserid, err := strconv.Atoi(vars["other_user_id"])
	if err != nil {
		fmt.Println(err)
	}
	u2.Userid = OtherUserid
	err = u2.GetUserById()
	if err != nil {
		reason := fmt.Sprintf("cannot get user by id %d\n", u2.Userid)
		json.NewEncoder(w).Encode(reason)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var s struct{
		State	string
	}
	err = decoder.Decode(&s)
	if err != nil {
	}
	if s.State != Liked && s.State != Disliked {
		err = json.NewEncoder(w).Encode("wrong state \n state should be liked or disliked")
		return
	}

	currentState := ""
	switch s.State {
	case Liked:
		u1LikeU2 := Contains(u2.Userid, u1.Liked)
		u1DisLikeU2 := Contains(u2.Userid, u1.Disliked)
		u1MatchU2 := Contains(u2.Userid, u1.Match)
		if (u1LikeU2 != -1) || (u1MatchU2 != -1) {
			break
		}
		if u1DisLikeU2 != -1 {
			u1.Disliked = append(u1.Disliked[:u1DisLikeU2], u1.Disliked[u1DisLikeU2 + 1:]...)
		}

		u2LikeU1 := Contains(u1.Userid, u2.Liked)
		if u2LikeU1 != -1 {
			u1.Match = append(u1.Match, u2.Userid)
			u2.Match = append(u2.Match, u1.Userid)
			u2.Liked = append(u2.Liked[:u2LikeU1], u2.Liked[u2LikeU1 + 1:]...)
			currentState = Matched
		} else {
			u1.Liked = append(u1.Liked, u2.Userid)
			currentState = Liked
		}
		u1.UpdateUser()
		u2.UpdateUser()

	case Disliked:
		u1LikeU2 := Contains(u2.Userid, u1.Liked)
		u1MatchU2 := Contains(u2.Userid, u1.Match)
		u1DisLikeU2 := Contains(u2.Userid, u1.Disliked)
		if u1LikeU2 != -1 {
			u1.Liked = append(u1.Liked[:u1LikeU2], u1.Liked[u1LikeU2 + 1:]...)
		}
		if u1MatchU2 != -1 {
			u1.Match = append(u1.Match[:u1MatchU2], u1.Match[u1MatchU2 + 1:]...)
			u2MatchU1 := Contains(u1.Userid, u2.Match)
			u2.Match = append(u2.Match[:u2MatchU1], u2.Match[u2MatchU1 + 1:]...)
		}
		if u1DisLikeU2 == -1 {
			u1.Disliked = append(u1.Disliked, u2.Userid)
		}
		u1.UpdateUser()
		u2.UpdateUser()
		currentState = Disliked
	}
	re := make(map[string]string)
	re["userid"] = strconv.Itoa(u2.Userid)
	re["type"] = UserType
	re["state"] = currentState

	err = json.NewEncoder(w).Encode(re)
	if err != nil {
	}
}
