# create user
curl -XPOST -d '{"name":"Alice"}' "http://localhost:8080/users"
curl -XPOST -d '{"name":"Bob"}' "http://localhost:8080/users"
curl -XPOST -d '{"name":"Chris"}' "http://localhost:8080/users"
curl -XPOST -d '{"name":"David"}' "http://localhost:8080/users"
curl -XPOST -d '{"name":"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbb"}' "http://localhost:8080/users"

# list all users
curl -XGET "http://localhost:8080/users"

# make relation
curl -XPUT -d '{"state":"liked"}' "http://localhost:8080/users/1/relationships/2"
curl -XPUT -d '{"state":"liked"}' "http://localhost:8080/users/1/relationships/3"
curl -XPUT -d '{"state":"liked"}' "http://localhost:8080/users/1/relationships/4"
curl -XPUT -d '{"state":"disliked"}' "http://localhost:8080/users/2/relationships/3"
curl -XPUT -d '{"state":"liked"}' "http://localhost:8080/users/2/relationships/1"

# get relationships of one user
curl -XGET "http://localhost:8080/users/1/relationships"

aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbcc